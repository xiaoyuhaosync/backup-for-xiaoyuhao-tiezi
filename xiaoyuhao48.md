
##### 我家的小雨浩  
&emsp;那是在一个雨天，我遇见了一只猫  
  
小小的，很瘦弱，花毛，可是又不太好看  
  
那个时候的我，正处于一场大事件的余波后，每天浑浑噩噩的不知道做些什么，父母也在善后，妹妹则躺进了医院，家里只剩我一个人而已  
  
可能是出于同病相怜之类的说不清道不明的情绪吧，我去小卖部买了火腿肠喂它，它一开始还不相信我突如其来的好意，在远处喵喵的叫着，似乎忍得很辛苦，它好像很久没吃过东西了，连叫出来的声音都是小小的  
  
我苦笑了一下，掰了一小块扔过去，小猫半信半疑地闻了好久，开始小口小口的吃起来  
  
自那以后就和它混熟了，我经常在下午的时候出门，手里攥着偷偷买的猫粮和火腿肠，在马路边等它，有时候它会来的晚一些，我也不着急，趁着它狼吞虎咽的时候摸着他的脑袋，有些时候我会对它哭笑着说要是你通灵的话就保我平安吧，日子真的不太平……  
  
那一天我过马路，因为前一天失眠了，昏昏沉沉的，连看都没看直接冲向机动车道，这时我脚下一疼，摔倒了。  
  
在我缓过劲来之后再看向前方，那只被我喂的很大只的花猫被车子撞飞了了好几米远……  
  
那天晚上我梦到它，它舔着我的手说  
  
我不能保你平安  
  
但我有九条命，分你一条  
  
那之后就没怎么养过小动物了，一来我实在不会养，除了喂食什么也不会的主人很容易把宠物养死；二来……  
  
再也不想看到那样的情形了吧……  
&emsp;1504楼  
&emsp;2020-03-14 21:46>
> ###### 大蒙古球:  
> 泪目<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon9.png" >
>
> ###### 风满袖ლ:  
> 写的真好
>
> ###### 春日野-悠-:  
> 好梦幻<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon92.png" >
>
> ###### 井上敏树😈:  
> 泪目<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon9.png" >
  
***
##### 小怪兽<img src="//tb1.bdstatic.com/tb/cms/nickemoji/3-1.png" class="nicknameEmoji" style="width:13px;height:13px"/>绘梨衣  
&emsp;物皆有灵  
但为知己者通灵  
君与之于善  
彼报君以命  
流浪的它拥有的并不多  
付出自己的一切乃至于一生  
以报你施自不觉中的温柔  
这或许便是猫所明白的  
愿以此生报君恩吧🌸  
&emsp;1505楼  
&emsp;2020-03-14 22:59  
***
##### 鸿飞202-  
&emsp;看了新的故事，有点刺激<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1506楼  
&emsp;2020-03-14 23:38  
***
##### 梦宸思臻  
&emsp;凌晨暖帖  
&emsp;1507楼  
&emsp;2020-03-15 01:09  
***
##### 紫妈八千岁<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-1.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;早安  
&emsp;1508楼  
&emsp;2020-03-15 06:42  
***
##### Trtctl<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-6.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;阿浩我很久没来了，早上好<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon66.png" >  
&emsp;1509楼  
&emsp;2020-03-15 06:45  
***
##### 遥之穹<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;早上好  
&emsp;1510楼  
&emsp;2020-03-15 07:20  
***
##### vivo啥时间  
&emsp;早上好啊  
&emsp;1511楼  
&emsp;2020-03-15 07:45  
***
##### 新人轻一点<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;来了来了，各位早上好  
&emsp;1512楼  
&emsp;2020-03-15 08:23  
***
##### 新人轻一点<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;每日暖贴（1/1）  
&emsp;1513楼  
&emsp;2020-03-15 08:24  
***
##### 我家的小雨浩  
&emsp;Je suis désolé.  
&emsp;1514楼  
&emsp;2020-03-15 10:00>
> ###### 大蒙古球:  
> 为什么要道歉？Lz做了什么？说出来可能会好些，mon ami fidèle<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f28.png?t=20140803" >
>
> ###### 热情的纪元😄:  
> ？怎么了
>
> ###### 怎么七天:  
> 怎么了又
>
> ###### 呀嘎嘎嘎嘎嘎4:  
> ？
>
> ###### 我放弃了思考º:  
> 过去了，没关系的
>
> ###### 永远爱朝武芳乃:  
> 我看不懂<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon9.png" >
>
> ###### 我家的小雨浩:  
> 嗯
>
> ###### 我家的小雨浩 回复 我放弃了思考º: 
> 谢谢考哥
>
> ###### 悠穹永相随💍:  
> 楼主为什么道歉<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon66.png" >
>
> ###### 花卿依º 回复 我爱茶道🎅: 
> 好亲切的法语啊！Pourquoi tu excuse？
  
***
##### ENDL玖  
&emsp;祝你们与子偕手，白头到老。  
&emsp;1516楼  
&emsp;2020-03-15 13:07  
***
##### 我家的小雨浩  
&emsp;中午和丫头下楼在草坪上打了羽毛球，说实话我都不知道看哪个球……真的是绮丽的风景<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
还有不知道为什么今天的丫头异常的温柔哦，连和我一起洗澡这样的请求都答应了<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon3.png" >  
&emsp;1517楼  
&emsp;2020-03-15 13:29>
> ###### 江南老🐶🐶:  
> <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### ENDL玖:  
> 楼主，我有点酸了
>
> ###### 宏电子64:  
> 同是16岁的我现在柠檬的一批……<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon16.png" >
>
> ###### 骚话梓川💕 回复 宏电子64: 
> 浩爷16吗？<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >（萌新混脸熟）
>
> ###### ☞十字战争☜:  
> 你们不是已经一起洗过了吗？应该很平常才对啊<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 我家的小雨浩 回复 梅普露✨✨: 
> 性骚扰的评论我见一个删一个哦<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon16.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 我家的小雨浩 回复 梅普露✨✨: 
> <a href=""  onclick="Stats.sendRequest('fr=tb0_forum&st_mod=pb&st_value=atlink');" onmouseover="showattip(this)" onmouseout="hideattip(this)" username="hfar_ray" portrait="tb.1.d2c6f6cc.oLaubF-kXng-AvJzGomswQ" target="_blank" class="at">@🌐網上沖浪</a>
>
> ###### 🌐網上沖浪 回复 我家的小雨浩: 
> 给了一天，完事了
>
> ###### 我家的小雨浩 回复 💍小雨的午夜💕: 
> 住口！无耻老贼！丫头的身材不要随便乱报！
>
> ###### 💍小雨的午夜💕 回复 我家的小雨浩: 
> 好好好<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***
##### RX<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-2.png" class="nicknameEmoji" style="width:13px;height:13px"/>SV  
&emsp;<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1518楼  
&emsp;2020-03-15 13:37>
> ###### 没有上镜心💫:  
> 你怎么来了<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
  
***
##### 永远爱朝武芳乃  
&emsp;当然是看好看的球啦<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1519楼  
&emsp;2020-03-15 13:45  
***
##### 大蒙古球  
&emsp;又被喂了一大口C₆H₈O₇<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon33.png" >酸死了<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1520楼  
&emsp;2020-03-15 15:18>
> ###### 大蒙古球 回复 浪一绝世: 
> 哈哈哈哈氟锑磺酸警告
  
***
##### Kongou_光子呀  
&emsp;我又咬了一口手上的柠檬<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f33.png?t=20140803" >  
&emsp;1521楼  
&emsp;2020-03-15 15:59  
***
##### iowthts  
&emsp;看着lz的故事，手上的柠檬突然就不香了<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1522楼  
&emsp;2020-03-15 16:30  
***
##### <img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-2.png" class="nicknameEmoji" style="width:13px;height:13px"/>面码酱<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-2.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;暖贴.....  
&emsp;1524楼  
&emsp;2020-03-15 17:18  
***
##### <img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-5.png" class="nicknameEmoji" style="width:13px;height:13px"/>pixiv<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-5.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;一起洗澡的话....注意压强<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1525楼  
&emsp;2020-03-15 18:15
>
> ###### 故0929😇 :  
> 热乎的  捕捉大佬          
>
>
> ###### 我家的小雨浩 :  
> ***……极限压枪二十分钟<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon72.png" >          
>
>
> ###### 春日也穹💤 回复 我家的小雨浩 :  
>这都能压住 厉害厉害 莫非是穿着泳衣洗的？<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon16.png" >          
>
>
> ###### 我家的小雨浩 :  
> 回复 春日也穹💤 :全果<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >          
>
>
> ###### 龙腾的反抗者 回复 我家的小雨浩 :  
>这都能压住莫非是用的凉水<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >          
>
>
> ###### 我家的小雨浩 回复 龙腾的反抗者 :  
>在浴缸里一起泡的<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >          
>
>
> ###### TOP一MR :  
> 第一反应在思考和压强有啥关系的我...看了评论一脸蒙          
>
>
> ###### 永远爱朝武芳乃 :  
> 这也能压住？！，该不会不举吧<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >          
>
>
> ###### 龙腾的反抗者 回复 我家的小雨浩 :  
>小伙子我怀疑你不大行啊<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >          
>
>
> ###### 龙腾的反抗者 回复 TOP一MR :  
>和压强有很大关系          
>
>
> ###### 我家的小雨浩 回复 龙腾的反抗者 :  
>你们啊……<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon16.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><a href="http://tieba.baidu.com/i/sys/jump?un=永远爱朝武芳乃" onclick="Stats.sendRequest('fr=tb0_forum&st_mod=pb&st_value=atlink');" onmouseover="showattip(this)" onmouseout="hideattip(this)" username="永远爱朝武芳乃" portrait="tb.1.6dd01836.fyqZO0AAP-RUMYZ3zne2Fg" target="_blank" class="at">@我爱茶道🎅</a>        
>
> ###### 呀嘎嘎嘎嘎嘎4 :  
> 我都不知道你们在聊啥<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >        
>
> ###### TOP一MR 回复 龙腾的反抗者 :  
>靠压强压枪<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >        
>
> ###### 永远爱朝武芳乃 :  
> <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >        
>
> ###### 风满袖ლ :  
> <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >        
>
> ###### Nodgod🎓 :  
> <img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" ><img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" ><img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >        
>
> ###### 還真手機◆😁 :  
> 😂😂😂😂😂        
>
> ###### NOAHemmmme :  
> <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >        
>
> ###### 洛拾叁º :  
> 我真的很好奇你是靠什么压住的<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >        
>
> ###### 🎅火火 :  
> 该不会是x无能吧，™都压住了/<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >保命          
***
##### 悠远于天穹<img src="//tb1.bdstatic.com/tb/cms/nickemoji/3-18.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;又是故事？  
&emsp;1526楼  
&emsp;2020-03-15 19:07  
***
##### 我家的小雨浩  
&emsp;晚上在伯伯家吃饭……不得不说伯伯的手艺真的是绝了QAQ，蜂蜜烤鸡和蜂蜜烤鱼让我放弃了营养控制，饭后的甜点是香蕉船……上面还撒了果仁和碎巧克力，简直就像宣告着自己热量爆炸一样……每颗冰淇凌上还有车厘子QAQ……  
我不做人啦，伯伯！让我都吃光！  
另外丫头在餐后偷吃了一点酒心巧克力，现在开始在我怀里撒娇了w  
就像小女孩一样啊，不对这么形容的话感觉更像女儿系角色唉<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon72.png" >  
&emsp;1527楼  
&emsp;2020-03-15 19:31>
> ###### 苏醒赛迦队长:  
> 你也不做人了？乔纳森觉得很淦<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f16.png?t=20140803" ><img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
>
> ###### 永远爱朝武芳乃:  
> 妹妹（x） 女儿（✓） <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 悠穹永相随💍:  
> 芜湖～
>
> ###### 呀嘎嘎嘎嘎嘎4:  
> 觉醒了一些奇怪的东西<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 风满袖ლ:  
> 女儿<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***
##### 贴吧用户_Q5JU9MM  
&emsp;顶  
&emsp;1529楼  
&emsp;2020-03-15 20:16  
***
##### 新人轻一点<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;看着自己的表弟和四个6岁的侄女，又看向了大我十岁的表姐表哥，默默的留下了眼泪  
&emsp;1531楼  
&emsp;2020-03-15 20:24  
***
##### 有被笑道<img src="//tb1.bdstatic.com/tb/cms/nickemoji/2-30.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;嗯，虽然写的不错👍，但我还是要发这个图<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon66.png" >  
<img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=655db5c6b3c27d1ea5263bcc2bd4adaf/3bb1815494eef01f8b4b1fa7f7fe9925bc317d37.jpg" size="26999" width="374" height="340" size="26999">  
&emsp;1532楼  
&emsp;2020-03-15 20:25  
***
##### Nodgod<img src="//tb1.bdstatic.com/tb/cms/nickemoji/4-12.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;每日柠檬  
&emsp;1533楼  
&emsp;2020-03-15 20:26  
***
##### 我家的小雨浩  
&emsp;前三张，侧脸，正脸，45度仰角，最后一张，我把头发撩起来了……我恨百度的AI算法……<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon9.png" ><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=ee378a634b6034a829e2b889fb1349d9/b5cd3787e950352ab14e266a4443fbf2b2118b67.jpg" size="112145" changedsize="true" width="560" height="746" size="112145"><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=e2028f40dffcc3ceb4c0c93ba245d6b7/1fd17bc6a7efce1b3804cba8b851f3deb48f6567.jpg" size="90341" changedsize="true" width="560" height="746" size="90341"><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=6c5775fb9526cffc692abfba89014a7d/b4f9923df8dcd10061b3fb43658b4710b9122f67.jpg" size="95378" changedsize="true" width="560" height="746" size="95378"><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=2b55c44da9096b6381195e583c338733/fcb3a4773912b31b9142ce4b9118367adab4e160.jpg" size="104324" changedsize="true" width="560" height="746" size="104324">  
&emsp;1534楼  
&emsp;2020-03-15 21:59>
> ###### 我家的小雨浩:  
> 我是男的！我是男的！我是男的！<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon6.png" >
>
> ###### TOP一MR:  
> 楼主你的还是你 妹的
>
> ###### 核瑔dar🌌:  
> 这在哪看？
>
> ###### 我家的小雨浩 回复 TOP一MR: 
> 我的……我妹比我高……
>
> ###### 我家的小雨浩 回复 核瑔dar🌌: 
> 微信小程序 搜索百度AI
>
> ###### 我家的小雨浩 回复 TOP一MR: 
> ？
>
> ###### 德骨主治医生º:  
> 你这是在……Cosplay？
>
> ###### 核瑔dar🌌 回复 我家的小雨浩: 
> 谢谢
>
> ###### TOP一MR 回复 我家的小雨浩: 
> 眼瞎发错了..这玩意是没有未成年选项吗，识别完全是成年的
>
> ###### 梦宸思臻:  
> 我。。。本来是男性55分，化了个妆，女性84分？！？我惊了
  
***
##### 我家的小雨浩  
&emsp;第一张是我重新拍的，第二张和第三张是丫头的……为什么我会比自己的妹妹还像女人啊岂可修！！！<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon9.png" ><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=0881f00e073853438ccf8729a312b01f/8d3676f40ad162d9cf78617e06dfa9ec8b13cdf8.jpg" size="102500" changedsize="true" width="560" height="746" size="102500"><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=25cb02fa49df8db1bc2e7c6c3922dddb/666e382ac65c10382ae8cda0a5119313b17e89f8.jpg" size="90474" changedsize="true" width="560" height="746" size="90474"><img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=16b3a179c30735fa91f04eb1ae500f9f/526a5b4e9258d1090e75aad0c658ccbf6d814df8.jpg" size="103247" changedsize="true" width="560" height="746" size="103247">  
&emsp;1536楼  
&emsp;2020-03-15 22:14>
> ###### 大蒙古球:  
> 凡事要往好处想<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >这样不就有光明正大的不修边幅/熬夜/不理头的理由了<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
>
> ###### 悠穹永相随💍:  
> 颜值87👀哇，可能是楼主脸挺白的吧（像女人）<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 新人轻一点♂:  
> 颜值87。。。<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***
##### 永远爱朝武芳乃  
&emsp;比女人还女人<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;1537楼  
&emsp;2020-03-15 22:22  
***
##### 還真手機<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-5.png" class="nicknameEmoji" style="width:13px;height:13px"/><img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-25.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;18岁的我望了望那3岁的妹妹摇了摇头。  
&emsp;1538楼  
&emsp;2020-03-15 22:29>
> ###### 横岭雨至:  
> 18岁的我看完后望了望那6岁的妹妹也摇了摇头<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
>
> ###### 绝云幽影💫 回复 横岭雨至: 
> 18岁的我看了看那9岁的弟弟也摇了摇头
>
> ###### 妹控没有妹🐶:  
> 十五的我看了看空气也摇了摇头
>
> ###### 新人轻一点♂:  
> 18岁的我看了看5岁的表弟和四个6岁的侄女摇了摇头<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 常盘台的舰长💨:  
> 18岁的我想了想那远在吉林5岁的表妹摇了摇头<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f33.png?t=20140803" >
>
> ###### YUKIººº:  
> 17岁的我望了望那三岁的妹妹和零岁的弟弟摇了摇头
>
> ###### 我不是破烂😂😂:  
> 15岁的我望着13岁的妹妹摇了摇头
>
> ###### 🎅火火:  
> 14的我看了看空气摇了摇头
>
> ###### 氵王🌝:  
> 12的我看了下10和6的两个妹妹貌似<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***